package com.example.demo.demoplugin;

import com.intellij.openapi.vfs.newvfs.BulkFileListener;
import com.intellij.openapi.vfs.newvfs.events.VFileEvent;
import org.jetbrains.annotations.NotNull;

import java.util.List;


public class AppBulkFileListener implements BulkFileListener {

    @Override
    public void after(@NotNull List<? extends @NotNull VFileEvent> events) {
        for (VFileEvent fe : events) {
            System.out.println("file updated event : " + fe.getFile().getName());
        }
    }
}
