package com.example.demo.demoplugin;

import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.FileEditorManagerEvent;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AppFileEditorManagerListener implements FileEditorManagerListener {

    Logger logger = LoggerFactory.getLogger(AppFileEditorManagerListener.class);

    @Override
    public void fileOpened(@NotNull FileEditorManager source, @NotNull VirtualFile file) {
        System.out.println("File opened event : " + file.getName() + " in project : " + source.getProject().getName());
    }

    @Override
    public void fileClosed(@NotNull FileEditorManager source, @NotNull VirtualFile file) {
        System.out.println("File closed event : " + file.getName() + " in project : " + source.getProject().getName());
    }

    @Override
    public void selectionChanged(@NotNull FileEditorManagerEvent event) {
        System.out.println("File selected change event occured in project :  " + event.getManager().getProject());
    }
}
